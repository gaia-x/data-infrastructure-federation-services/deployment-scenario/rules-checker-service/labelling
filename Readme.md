# Labelling

# Getting started
```
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/labelling.git
npm install
npm start
```

## Description API

API labelling is a compliance and labeling technology process that automates all the checks necessary to give a service a specific label. Labelling will make it possible to classify services according to different criteria, such as:
- their contractual governance
- their data protection
- their security
- their portability
- their european control
The label 3 being the highest confidence index and the label 0 being the lowest confidence index. For more details on the criteria to be validated to respect a label please refer to https://gitlab.com/gaia-x/technical-committee/federation-services/security-and-norms/-/blob/main/ComplianceService.md#54-labelling

## APIs 

HTTP POST: http://localhost:3001/

* Input example:
```
{
    "@context":[
       "https://www.w3.org/2018/credentials/v1"
    ],
    "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vp/3318512748ebd2924cf50e2b3efee377e76038e346a4d3ad266d47dfcf1aa5df/data.json",
    "@type":[
       "VerifiablePresentation"
    ],
    "verifiableCredential":[
       {
          "@context":[
             "https://www.w3.org/2018/credentials/v1"
          ],
          "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/5d61abb4ff29f50abbf4d21e0c3696dd5b2693fe3deaae3275f2d500ecc8b5c1/data.json",
          "issuer":"did:web:dufourstorage.provider.gaia-x.community",
          "@type":[
             "VerifiableCredential"
          ],
          "credentialSubject":{
             "@context": {
                "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
             },
             "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/located-service-offering/9e9caae3a13c28cad5371c3703705688191d0a082e33ef87d8a79fff9acb8760/data.json",
             "@type":"gax-service:LocatedServiceOffering",
             "gax-service:isImplementationOf":{
                "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/service-offering/16ea3d9c5c9ef155dfee355366b44fc7119afdbeb84b809d54518b253d67310d/data.json",
                "@type": "gax-service:ServiceOffering"
             },
             "gax-service:isHostedOn":{
                "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/location/f17220a5c93bdc04451b1253015b4e2c2dc1cd532b9e4e6e75f13f39080024f6/data.json",
                "@type": "gax-service:Location"
             },
             "gax-service:hasComplianceCertificateClaim":[
                {
                   "@id": "did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/135bbe48a4b6191e649d459d9813e47f2ef7c89aa536b6f58c0e6b95e49eef3c/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/19d3cf2179ae1a58079b08a8d5be37ed1c6be1d43ddaebffd393a35fcf3dd9b8/data.json",
                   "@type": "gax-compliance:ComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/135bbe48a4b6191e649d459d9813e47f2ef7c89aa536b6f58c0e6b95e49eef3c/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/19d3cf2179ae1a58079b08a8d5be37ed1c6be1d43ddaebffd393a35fcf3dd9b8/data.json",
                   "@type": "gax-compliance:ComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/e0494cedac828261e38870542b59cb22115d96a44a995064fe82308d759d43b4/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/b82fa45ddf367ff84f02423859ab671f7dc2145243b294dab5e3a11f383e850a/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/939edf544d0699269c8bbdf7645f774c0ce229fad84073b242f1e04f93199427/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/6f2ffa2f6839d7bd8ede1773b5f4743f282d739edfd0f1ab83f31858898f7449/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/3a67340a10ae73981a97f8cb95528bb4198ec7999411a67dc237deff8b6d036e/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:kpme.auditor.gaia-x.community:participant:344bff8b2788383a072daddf6088c90e6b7b2f026e992a7267ff5094ad6c13eb/third-party-compliance-certificate-claim/3e59f84b754278378b80b3abce50494ff4b88cbebec6cfee76637e2fd63bdc8e/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/3e58c345412d2902a8fd12958e605dc08e41422df272e420f823b41f69b3c034/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:sap.auditor.gaia-x.community:participant:7a9c94a8c201bcc22857e49fae92628458cbe2648ab0d3d747d7ed656ca20eb2/third-party-compliance-certificate-claim/fd109e557785d6012b59a7d5a6f7a39d7981751832f43af946c8ae3607777545/data.json",
                   "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/529966a964cfc44f7c7057b7acc2bcea9a28525bae01f858e7ae3a2d7681f238/data.json",
                   "@type": "gax-compliance:ComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/28b908196ae876931baa50e5d425b3e16a607825b64331fa2251d7687910eacf/data.json",
                   "@type": "gax-compliance:ComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/f514bd82c3c94c472bfadc0252128cc09fe55a91637262b8f9885f9b4e76c9c2/data.json",
                   "@type": "gax-compliance:ComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/9fa281f431444e6b140de58a0c1bb6dfd7175b9c492eaf498b35a50e9b410e77/data.json",
                   "@type": "gax-compliance:ComplianceCertificateClaim"
                },
                {
                   "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/38067b6d2755e8d36a8a609b20591892e2d360a5a55780595f337af6c1268d35/data.json",
                   "@type": "gax-compliance:ComplianceCertificateClaim"
                }
             ],
             "gax-service:hasSelfAssessedComplianceCriteriaClaim": {
                "@type": "gax-compliance:SelfAssessedComplianceCriteriaClaim",
                "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/self-assessed-compliance-criteria-claim/8baf6ee12b8af2b192016e9c9a3befb71dcf9a313e4b9faf59d75840eb163818/data.json"
             }
          }
       },
       {
          "@context":[
             "https://www.w3.org/2018/credentials/v1"
          ],
          "@id":"https://ruleschecker.abc-federation.gaia-x.community/vc/xxx",
          "@type":[
             "VerifiableCredential"
          ],
          "credentialSubject":{
             "@context":{
                "gax-compliance":"https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#"
             },
             "@id":"did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/third-party-compliance-certificate-claim/135bbe48a4b6191e649d459d9813e47f2ef7c89aa536b6f58c0e6b95e49eef3c/data.json",
             "@type":"gax-compliance:ThirdPartyComplianceCertificateClaim",
             "gx-compliance:hasComplianceAssessmentBody":{
                "@type":"gax-compliance:ComplianceAssessmentBody",
                "@id":"did:web:lne.auditor.gaia-x.community:participant:375ac5ddbc6f9db93c90b71ffbcfe6697d0baf44306d980c1c21afff1f26cc18/data.json"
             },
             "gx-compliance:grantsComplianceCriteria":[
              {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/b272ac3eba05f1b37802818a46eff6b6c5edb34df2dfa54430519752d5aafa63/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/996d747c5c86cdda911eb524b584e4609262787ca202a2aca9e56b6ebe6fd9ef/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/a5ad1d8a9f50565e6bb1ead2dd35414930aff9b601423625ef88db452ccb1c46/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/60fa3eb419b59b4a268f23f3a91edc6531cb6bf27271040c4071b006dc381add/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/66d463e6c7a02f666ec1676401ce9cedac39dd6c06dd445f683416dbd746dd24/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/d36f07d0d2cab49f3f65ae073a2188825701cfdac3474cf29178a3b9aaf33aa2/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/14cc656193a13059ce8f396e8390363e2a06e80782fd9c9920ad8a990eae6572/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/62118daa66dec6045b47734998f44ef24f2962683ebc9a481d5ce9669071f38c/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/fe75fa286fc83bd1a50514d1edbf2d3b6e98e1b071f53ee8e8f918dfb1a05960/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/06c2146bab4a4f6855c1937a6b3dfc7e3624801c34885fe65194687eeb39597c/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/a664a263aca29c6562667950df49efac86ea9fa3d054fbd358699033c62d94ab/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/fbedad10c6aedc594f3bb8cfc3f490de451de172b5884e4ac9de239d1cf43da8/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/87598bf7dd6b23a05698057ab6be23157e8b8d24c7378c6b8d3175c8622a3b88/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/38a819c9216821493dd6246311b7d90aaf7d099a257cfe80bb4ae9146081eaa5/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/ed0ec30bc14b2e49e6998999ae025d1fa7f6457060e29b4d4921069a75f7d634/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/5838459d73296aea7ea6a251ad7d00987b2e0f8ba27f35ae982d1cd05e9df04b/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/9dbd187fb282e47c33508785be131da14dace7ee655c6fb11cd3c5843cb03c5d/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/120af2bd5cf3a4c6f1d4172630a6ac8e7440f1ab11793d45a2f15bc435b681c4/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/1f31b2d49e7027f3fe3f5adbcd3b9e525ac54b0fee14e479d72608794b179a82/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/9657bab3aa7e77dd7702d45d60819aa94681642c122c01b4429294e010a0915d/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/44486f565f02e5d31dac6f61093bf61a2ddff05e6de14fbb5c73b45e085d796d/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/80ce7a0cc21b1586f010e5ed866ec68c7024a71a7830448078ce149014f7b3ea/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/ef05ea348bd0dc295c2ab0efdc055485a0e040a126fe5e6f0e8fb97df2fa5937/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/290fff7d7708d9dcc4061344d56340f79f491f5e09ffeecf8ce74f36e5d1537b/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/3389064ffa80e8adccef38e64d271ba9c8be356890e9d4f989e5a8a019021ac4/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/62f47d7dbc438ab84659b216fe7917d5123935a5cd1fc153f01cb48524ca2512/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/080749a06cdb3d9968820b061022c82f1c6848987822869a05a1f0bd7483bd1c/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/1b1c5373dee06334d78653ded4c471ca288250ee5adf40dde88592890411d13d/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/196cc0ad3f280b58973c07093378ba877c28fbd89164c5cfef96757c2de1ee6b/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/fb7f641bf5a02029ebcbf6424cf4265cede113df0368789eb71f8962dfcbd39b/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/54f1389a18b0e1699e33208db67a17b81e086aff172908fb315c8a8080745695/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/956f6ff5bfc9f333041934e0532d03a50b6303420d8de259c3d00840c49a174d/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/059d7f787560f30acc8cbf3bbe6a36f69c26a620e344ba60f5e746e150178d03/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/a2bd5a6be70aafebbca2c54b7ded2145385fb81fc249279d0ef0d02d96d96c62/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/2781c5f8f4ac258bb2f2f9038e17547e9f32080918790a5daf2766b9452862e2/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/301c19291951a7b0d21259a308793e789321447eba43f98fc2cac625ab97e747/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/386e15bddba6ae67c977c381ca26b5a27cd645c085314397975c0e11929754c5/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/2b01971ef3c1f0f4b1c6c264ea17218c96a71af7769d8bef693c9f761ad0ad31/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/8a47437b0de44e2fbecdaf03503e41601015317849ac5e330133faea6251899a/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/509275128a2b2036ae2d7107a77817b3b3c32b0e116632cba8df5fdfbc386422/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/198318c5517f8a4c62cbb4927b4d579e1791860c23b5a56870e0cc61e65a1ecb/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/c6700813f6578e10d69e8cc7c4c7c35f9b28a18966c39ebd109e85a38022cfa0/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/660996859a7350f21a39c691431a55367acdfb0df86c66104ac74571224664dc/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/3e5cb4cf7266a4ad42c787196fdd61d7f505544d2c316716a461b76bbae466d6/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/572eb736fd40e0772e0b79c54ced5ee374eeb4e85a5ec4bee9c3fb27fc68ad2e/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/7c19ccee786f1a73804323114c771ae98fa8f2657a3cf9442e2a93059eba90b9/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/49c274c1bbf962a3f9a96c7f05f43a516310f5b3323c972eade660c3f75026fe/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/6f70b3987c46695af68d26288e95cd75c7a8869570c4a135d90bd2fbb42440ea/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/9f9d8e5c2b2c294688515279cb9b903d1f287957275b4b818f08ed0036ec6d79/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/d09b26b54a94b54078e310279516f8983daefacd5adca7724c252eab2772136f/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/2a534ed4ba425430049f77d95be338c907ad25f09c03f913b7ecee4ba9ea682d/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/75cffc744a6d84f259ced7f74634f937255ae688c8629425ef38162c0cfba9b1/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/057d78ac024071781452c5552f709c64583ece6ae21333fafc5546dca5feb0ca/data.json"
            },
            {
                "@type": "gax-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/0abfed593985167c613f6974a89dc6fdaadfd58e4711cf5c0124d9ee02916597/data.json"
            }
        ],
        "gx-compliance:hasAssessedComplianceCriteria": [
               {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/ea307320071d2c97ba1fc264ac2b57fd571a30fd4bc08b85139ca3256a726b97/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/2ee23dc8f147f6e8e5707f92161b2695d25c2571af06f832d55eb3dcd01109ca/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/362213edf62b155d61e6d72c0dde5d7a01b982218eb5a2b00e7b1bb6015dee98/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/1f2872e3642e6d616bf6af0a3f4c14ef6ca6111e6854184cfbf3cb4ef88a15d5/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/1df51e3055940bad2b48205d85db6fc7968af811ce74a9383a99dd9bef548340/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/c754817b22a03a60efc60569ef8cf483fbf0df402d1f72c863967ab393e9b3af/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/10e1999e6107d991f3251a57252ca95b7564d7817f34ffcdb2520a1939746749/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/3cb671f9bb51069b27ff31208025553f6386e7d37a7429dd058a9b5924fe64fc/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/958b517ec144386f06c238dfafa801a4e719807da0a261ecdb1546cd1246a16c/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/5c6c37938ccec54ddbe569d1e429658d7c3a86b3cb91f5d7b9fa81b61efe6eba/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/9975196b786b14f3992114e5e1f1cd35befdb5e7fa439e2f94bef927f328ac49/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/18e49d1e6c56efb00f43a863c87d147e8598a450a683ab6e0db245b7d4d2af86/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/471b984f28f8a79f7c11f3e44fe298aab7152efc5f97d29fea6d45f2e9a0efd1/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/98d0eec88ce471c8a0485636b95b7b5f28a743131e84c79a937e8c92de7e330c/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/c51a27275cbb87b247d21b81ef16f585d114c22e48913eb6d9fb4f0e3aa01ba6/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/69e118feec07463d4e827aa345ee659ce32e5075e6ddcf8daff7027a99f8893c/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/25d78bede67bb195d69b9419808d74015c3d5f4a2d733c676a7244b3950ce1b9/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/843d0a4650499b09e37b5dd7759da4ddbc4f7d7098bc806149a8829f9ea0f315/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/36c42f80bc75fb96d66f64f1ae4f75798c7906f2a515adfae982367d3ace56dd/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/c9280867077e758137a1b6ce8010698e187a98e1ebf01fc28e64be76611528ad/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/53ff2d76126a8b7404fc769734043b3a24d5e7422029cb2666f2b8a4a11fb49b/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/12f11e3c592b4285d3f051f933a99517b997a543d5705d8717a40af6bc150b2a/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/e06d36d7b023d7b1da6ef0c9f8d451b8fb1ddecd4538e03d9c63fa3d1c59d990/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/74b947356bc21c2daaf7f0984b53a1493ed4678f53680d63830842bd22fe352f/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/f36bebe520dbf1736c3840769828d093791053c5aa6c6a46d1848595e00140bc/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/9e45260877e1c896508618cefd9f16c185e72dd89fba426eb3901eff167d2360/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/d604a332d9ca9c8f2b93fc779b35ec2e57818fdedba5d8cef260e69af524b8f2/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/db57c2316ae02bfb8c4b66368735234fb917995b91123775b164cbbbfa19ca6f/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/7bedfaaa1ed74be7de48eafac21cfbad4856c424f1638d1af0a2053d2cb227a9/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/85044fd562ef04649b9d5530e7f4ef248ae6ab81b3c9fa1f4fcd3847ce1c94f1/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/fb797087b3951edc3add77837cd5401cacc7f2fc206413ff3c9cbff563dd8f68/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/cde1519cd7f68cce2951987c72d75decf3626a403e89d458fbceb7ac46f06f5a/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/c4a921a3a3df7fa678c0a521a22a5dac0528bed85233719f71c7cbf52edf302a/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/6dcd080420ba9a1f7db82682395ed54aacdda5b6ba7c001eec61e2d19226dd2a/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/a1f306c65a2969c1233cb0142034c0ab21dcac4615f21ad45cba9a8e4e273653/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/273a31001e5ea054732d0983c44343612a339b6c5e9c5eb2b1209d77487ad94e/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/1d0535639c6c2e26790ac19a790ad547ac1b4d7c12bdd0f8dd4c3f8c726fc7e5/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/f5284af5d5bf3b1a80a90c65f65f3ff15fa15ca524085685bae85ca135d62782/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/733aca07e2d2e11cba61dc74e387d6ca289ee895e521c9c091141be15118c5ba/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/75571bb4c63239f0df7168e1a44ebdf278fd37738eff607cfb9049c7c2bd083b/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/cf15736ee674b6fbe9f32bc91a9a81a2b1b1135517a352037edfb0808cddba81/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/f223175e85cf168bd7df2858c32b5db5267772986299221f4bba5762bd8482c8/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/3c6be9b458b4402c708733302056fd55aa58d6f1e865b61b3bf03f3db6145e47/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/b388093c6117171072423b78a1d223caeb66d79266301e695df169d1ab391c79/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/d6c80de92f9426e57b637f56000530e5142a085abd704e63d708ad9eb68db5dc/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/ed6a2f7f46f6faacf8142fda2733383ae6b268ccbdadf3293fa4eeb35daf8091/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/33ffebcc9d1c490ddee3c16f039f801a035a8fe18f88262a7fd2d6b517b4c39e/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/26c3d521a326ee292fd20ce72f3dfffe0ffd290769283f2a2a7738e8a6847d40/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/bcc7f7d1ca3d08cc612e3fb3aacc4e3e9e57fd4f15fd6610fc1027f3cfd7ab10/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/dfb922e542fe48da698dfd1599053f203311ca650003a786e88cc392ae54b698/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/2808237322eab75f3d041469954dc61bcaab3153b63558d7f8f4449c4bfcc894/data.json"
              },
              {
                  "@type": "gax-compliance:ComplianceCriterion",
                  "@id": "did:web:gaia-x.community/compliance-criterion/bc3a08b25f3e84f6e0bbc06e03b038cc04c9d3eee2a654d956e98f6360e6baa0/data.json"
              }
            ]
          },
          "proof":{
             "type":"JsonWebSignature2020",
             "proofPurpose":"assertionMethod",
             "verificationMethod":"did:web:abc-federation.gaia-x.community",
             "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..G58eH2B0k7vCNUqbKkDmLX6ySd60l7lzaTR9IofY7RK1G_kWBdM_pGDjQiXdu7bxx_hU7NwmygHqQkcrnPbwtWwIMjZlkDit6NnmdePlYghGl9TBrJvxyXvWtgkcl6dJdUPrk0GY0sCnMvTf-O-hPV4Rd8qGh_a_qY7LJ4CAqnT26nvg-htdaMbiBub5grYj0kWHN4tQ0mBeWZgLHPMgqBnVEfuhmt-JwcEf6sBj7QtwYVQbXGHxWfzon1xXiGVWccvSPc7Dhf49KestMrOZfimCeiKWaK26R316JxZNFyrrviMDopQJDjpgmNsxR1Az1gKXQ1mO_C_gmmGgxoCGCg"
          }
       }
    ],
    "proof":{
       "type":"JsonWebSignature2020",
       "proofPurpose":"assertionMethod",
       "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
       "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..LQZbnJQq-Jh75N33D8MXFgiFPvvw1EFK5FuUXbsOgQfy9U_EEGxQ9D2LgnWIhTRWOBQl3IYvanH-2ggGlg3YhQlQpCedgc1XWci-E_a5tjZAyjg-oKqN265yqjw3xbMvfHUfcHyR-F4FgYANIxQNixHvv9Kuj-6sjAANi-KwLZnyDVrTxZ0jBS4VJoIXuGeHBETaE7mciiinBZjvbRjOv76MHEQJnzf4YcOngcKQtaSGYB5dXc4hxe7fEpe4q3kSeO2xmuCwwSMjpgijoTnIrYPMej9i3lnkjUusWIbkbQch4Bk2C9MCsxGR-wDUIey5VbVVP9eru3hpiEO4Qi2eig"
    }
 }
``` 

* Output example : 
```
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "id": "https://labelling.abc-federation.gaia-x.community/vc/15361a72-1516-4bc2-9ab3-704e7c91c3b6",
    "type": [
        "VerifiableCredential",
        "GrantedLabel"
    ],
    "credentialSubject": {
        "id": "did:web:ovhcloud.provider.gaia-x.community:participant:8384382c-77c2-45fe-9699-6edf603bbd52/granted-label/d95650a6-efaf-441f-ae50-2bfaeab9a54a/data.json",
        "@type": "gax-labelling:GrantedLabel",
        "vc": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/5d61abb4ff29f50abbf4d21e0c3696dd5b2693fe3deaae3275f2d500ecc8b5c1/data.json",
        "label": {
            "hasName": "Gaia-X label Level 2",
            "@id": "did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/8a9bae461da3b6945ece65b342fe14093509eda4494f5b5116a14d64736211b5/data.json"
        }
    },
    "issuanceDate": "2023-02-27T09:52:59.521Z",
    "expirationDate": "2023-08-26T22:00:00.000Z",
    "issuer": "did:web:abc-federation.gaia-x.community",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2023-02-27T10:55:55.336824+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..MaLFy7IaG10K90usBcKrNG6FVgBdVLFaNoc93Zi0bMl9qjpzJ_jFAZU12P5-JOGo8lyZ8ftPsP83-1rH4IYUZjuV3KdXiZJYVBizyP3JijLe8Xg0XwB95tvLX2fflXezTtpNJI9ff8ZJUY8ZQMCVV2V40NpxiZRdDMK7Uk0GsZAD7W1HtDDNQ2FMa3nWosimElW4Fv67hDCQLjOh-HdiVlISMi1lmBWQ_WEW6tIc7cuRjkwM5Fg4y29NpFk2YgG1pJSwFVHt-WCad43WoBWWLFzAh2mDLRBjwpAC8ypnLN9T9LNi5PAB5HVgdVPYva-f8k25maBuq9zT_xFxmoQp3g"
    }
}
```