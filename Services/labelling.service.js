const axios = require('axios')
const jsonpath = require('jsonpath')
const uuid = require('uuid')

class LabellingService {

    static async parseCrit(jsonld, tab = []) {
        try {
            const criteriaResults = jsonpath.query(jsonld, '$..["aster-conformity:hasComplianceCertificationScheme"]..["id"]');

            if (Array.isArray(criteriaResults) && criteriaResults.length > 0) {
                for (const criteriaUrl of criteriaResults) {
                    let criteriaUrlModif = criteriaUrl;

                    if (!criteriaUrl.startsWith("https://"))
                        criteriaUrlModif = criteriaUrl.replace(/:/g, '/').replace('did/web', 'https:/');

                    const response = await axios.get(criteriaUrlModif);
                    const res = jsonpath.query(response.data, '$..["aster-conformity:grantsComplianceCriteria"][*]["id"]');
                    tab = tab.concat(res);
                }
            } else if (typeof criteriaResults === 'string') {
                let criteriaUrlModif = criteriaResults;

                if (!criteriaResults.startsWith("https://"))
                    criteriaUrlModif = criteriaResults.replace(/:/g, '/').replace('did/web', 'https:/');

                const response = await axios.get(criteriaUrlModif);
                const res = jsonpath.query(response.data, '$..["aster-conformity:grantsComplianceCriteria"][*]["id"]');
                tab = tab.concat(res);
            }
        } catch (error) {
            console.error("Une erreur s'est produite:", error);
        }

        try {
            const criteriaSelfDeclared = jsonpath.query(jsonld, '$..["aster-conformity:hasAssessedComplianceCriteria"][*]["id"]');
            tab = tab.concat(criteriaSelfDeclared)
            return tab
        } catch (error) {
            return "You do not have criteria mentioned in your verifiable presentation";
        }
    }

    static async checkCritForLabel(crit, criteria = []) {
        try {
            if (!crit || !Array.isArray(crit) || crit.length === 0) {
                return {Error: "Error while getting criterion"};
            }

            crit.forEach(function (item) {
                criteria.push(item['id']);
            });
            return criteria;
        } catch (e) {
            return {Error: 'Invalid label object url'};
        }
    }

    static compareStringArrays(arr1, arr2) {
        if (!Array.isArray(arr1) || !Array.isArray(arr2)) {
            return {Error: "Either arr1 or arr2 is not an array"};
        }

        const stringsArr1 = arr1.map(item => (typeof item === 'string' ? item.toLowerCase() : '').trim());
        const stringsArr2 = arr2.map(item => (typeof item === 'string' ? item.toLowerCase() : '').trim());

        for (let str of stringsArr1) {
            if (!stringsArr2.includes(str)) {
                return {Error: "Comparison error between your criteria and the criteria of a label"};
            }
        }

        return true;
    }

    static async returnLabel(jsonld) {
        const checkCrit = await this.parseCrit(jsonld)

        const resLabel1 = await axios.get(process.env.URL_LABEL + "?level=1");
        const uriLabel1 = resLabel1.data[0]['id'];
        const critForLabel1 = await this.checkCritForLabel(resLabel1.data[0]['aster-conformity:hasRequiredCriteria'])

        const resLabel2 = await axios.get(process.env.URL_LABEL + "?level=2");
        const uriLabel2 = resLabel2.data[0]['id'];
        const critForLabel2 = await this.checkCritForLabel(resLabel2.data[0]['aster-conformity:hasRequiredCriteria'])

        const resLabel3 = await axios.get(process.env.URL_LABEL + "?level=3");
        const uriLabel3 = resLabel3.data[0]['id'];
        const critForLabel3 = await this.checkCritForLabel(resLabel3.data[0]['aster-conformity:hasRequiredCriteria'])

        const label1 = this.compareStringArrays(critForLabel1, checkCrit)
        const label2 = this.compareStringArrays(critForLabel2, checkCrit)
        const label3 = this.compareStringArrays(critForLabel3, checkCrit)

        let responseLabel
        let urlLabel
        switch (true) {
            case label3 === true:
                responseLabel = '3';
                urlLabel = uriLabel3
                break;
            case label2 === true:
                responseLabel = '2';
                urlLabel = uriLabel2
                break;
            case label1 === true:
                responseLabel = '1';
                urlLabel = uriLabel1
                break;

            default:
                responseLabel = '0';
                urlLabel = ''
                break;
        }

        if (responseLabel === "0") {
            return {Error: "You do not meet all the necessary criteria to be able to have a label. To know more about criteria and labels please refer to: https://gitlab.com/gaia-x/technical-committee/federation-services/security-and-norms/-/blob/main/ComplianceService.md#54-labelling"}
        }

        const id = uuid.v4();

        // Get CredentiaStatut
        let responseCredentialStatut;
        try {
            responseCredentialStatut = await axios.post(`${process.env.URL_REVOCATION_REGISTRY}/api/v1/revocations/statusEntry?issuerId=default`, {
                "credentialUrl": `${process.env.URL_REVOCATION_REGISTRY}/api/v1/revocations/credentials`,
                "purpose": "suspension"
            }, {headers: {'X-API-KEY': process.env.API_KEY_REVOCATION_REGISTRY, 'Content-Type': 'application/json'}});

            if (responseCredentialStatut.status !== 201) {
                console.log(responseCredentialStatut.status)
                return {Error: "Error while getting credential status"};
            }
        } catch (error) {
            console.log(error)
            return {Error: "Error while getting credential status"};
        }

        function getVcByType(vcArray, typeValue) {
            for (const vc of vcArray) {
                if (vc.credentialSubject && vc.credentialSubject.type === typeValue) {
                    return vc;
                }
            }
            return null;
        }

        const vcService = getVcByType(jsonld.verifiableCredential, "aster-conformity:LocatedServiceOffering");

        const vcID = vcService['@id'] || vcService.id;
        let federationURI = new URL(process.env.URL_VC_ISSUER).origin

        const returnVC = {
            "id": federationURI + "/granted-label/" + id + "/data.json",
            "@context": [
                "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity"
            ],
            "type": "aster-conformity:grantedLabel",
            "vc": vcID, //service offering

            "label": {
                "hasName": "Aster-X label Level " + responseLabel,
                "id": urlLabel
            },
            "proof-vc": jsonld.proof
        }

        try {
            return (await axios.put(process.env.URL_VC_ISSUER, returnVC, {
                headers: {
                    'X-API-KEY': process.env.USER_AGENT_API_KEY,
                    'Content-Type': 'application/json'
                }
            })).data
        } catch (error) {
            return {Error: "Error while signing GrantedLabel VC"};
        }
    }

}

module.exports.LabellingService = LabellingService
