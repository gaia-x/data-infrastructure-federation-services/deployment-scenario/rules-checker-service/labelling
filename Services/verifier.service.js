const axios = require('axios')

class VerifierService {

  // Check if the VP is valid (not empty, and VC not revoked or suspended)
  static async checkVP(vp) {
    if (vp == '' || vp.verifiableCredential == '') {
      return false;
    }

    for (let i = 0; i < vp.verifiableCredential.length; i++) {
      if (await VerifierService.checkCredentialStatus(vp.verifiableCredential[i].credentialStatus) == false) {
        return false;
      }
    }

    return true;
  }

  // Check if the VC is revoked or suspended
  static async checkCredentialStatus(credentialStatus) {
    try {
      const result = await axios.post(`${process.env.URL_REVOCATION_REGISTRY}/api/v1/revocations/verify`, {
        "credentialStatus": credentialStatus
      });
      if (result.data.suspended == true || result.data.revoked == true) {
        return false;
      }
      return true;
    } 
    catch (error) {
      console.log('Erreur :', error);
      return false;
    }
  }
}

module.exports = VerifierService
