const LabellingController = require("./Controllers/labelling.controller")
const fs = require('fs')
require('dotenv').config()

const express = require('express')

const app = express()

app.use(express.json({ limit: '100mb' }));
app.use(express.urlencoded({
  limit: '100mb',
  extended: true,
  parameterLimit: 100000
}));

app.post('/', LabellingController.LabellingController.getLabel)

app.get('/vc/:vc/', (req, res) => {
  res.json (JSON.parse(fs.readFileSync (process.env.PATH_VCS + req.params.vc + '.json')));
  res.end();
})


app.listen(8080, () => {
  console.log('HTTPS Server running on port 8080');
})