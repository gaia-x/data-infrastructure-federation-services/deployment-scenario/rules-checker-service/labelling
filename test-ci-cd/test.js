let checks =
    [
        {
        name: "Label 0",
        url: 'https://labelling.abc-federation.gaia-x.community/',
        testfile: './datas/label0.json',
        testResult: function (body) { return body.Error == "You do not meet all the necessary criteria to be able to have a label. To know more about criteria and labels please refer to: https://gitlab.com/gaia-x/technical-committee/federation-services/security-and-norms/-/blob/main/ComplianceService.md#54-labelling" },
        type: "post"
    },
    {
        name: "Label 1",
        url: 'https://labelling.abc-federation.gaia-x.community/',
        testfile: './datas/label1.json',
        testResult: function (body) { return body.credentialSubject.label.hasName == "Gaia-X label Level 1" },
        type: "post"
    },
    {
        name: "Label 2",
        url: 'https://labelling.abc-federation.gaia-x.community/',
        testfile: './datas/label2.json',
        testResult: function (body) { return body.credentialSubject.label.hasName == "Gaia-X label Level 2" },
        type: "post"
    },
    {
        name: "Label 3",
        url: 'https://labelling.abc-federation.gaia-x.community/',
        testfile: './datas/label3.json',
        testResult: function (body) { return body.credentialSubject.label.hasName == "Gaia-X label Level 3" },
        type: "post"
    }
    ];

export default checks;