const LabellingService = require('../Services/labelling.service');
const VerifierService = require('../Services/verifier.service');

class LabellingController {

    static async getLabel(req, res) {
        /*
        const isValide = await VerifierService.checkVP(req.body);
        if (!isValide) {
            res.status(400).json({Error: "Vp invalide or credentialStatus is revoked or suspended"});
            return {Error: "Vp invalide or credentialStatus is revoked or suspended"};
        }
        */

        const result = LabellingService.LabellingService.returnLabel(req.body);
        res.send(await result);
    }
}

module.exports.LabellingController = LabellingController



